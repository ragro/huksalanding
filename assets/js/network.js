var link="https://apiprod.huksa.com/";
var countries = ["93,AF,Afghanistan","54,AR,Argentina","61,AU,Australia","43,AT,Austria","880,BD,Bangladesh","32,BE,Belgium","975,BT,Bhutan","55,BR,Brazil","359,BG,Bulgaria","855,KH,Cambodia","237,CM,Cameroon","1,CA,Canada","56,CL,Chile","86,CN,China","57,CO,Colombia","385,HR,Croatia","420,CZ,Czech-Republic","45,DK,Denmark","1,DO,Dominican Republic","593,EC,Ecuador","20,EG,Egypt","679,FJ,Fiji","385,FI,Finland","33,FR,France","49,DE,Germany","30,GR,Greece","852,HK,Hong Kong","36,HU,Hungary","91,IN,India","62,ID,Indonesia","98,IR,Iran","964,IQ,Iraq","353,IE,Ireland","39,IT,Italy","81,JP,Japan","965,KW,Kuwait","60,MY,Malaysia","230,MU,Mauritius","977,NP,Nepal","31,NL,Netherland","64,NZ,New Zealand","47,NO,Norway","968,OM,Oman","92,PK,Pakistan","595,PY,Paraguay","63,PH,Philippines","48,PL,Poland","351,PT,Portugal","974,QA,Qatar","40,RO,Romania","7,RU,Russia","966,SA,Saudi Arabia","65,SG,Singapore","27,ZA,South Africa","34,ES,Spain","94,LK,Sri Lanka","46,SE,Sweden","41,CH,Switzerland","66,TH,Thailand","1,TT,Trinidad and Tobago","90,TR,Turkey","971,AE,U.A.E","380,UA,Ukraine","44,GB,United Kingdom","1,US,USA","263,ZW,Zimbabwe"];
var globalNetId; //edited by Nivedita, Ankit,Rohit

		function getUrlParam(param) {
			param = param.replace(/([\[\](){}*?+^$.\\|])/g, "\\$1");
			var regex = new RegExp("[?&]" + param + "=([^&#]*)");
			var url = decodeURIComponent(window.location.href);
			var match = regex.exec(url);
			return match ? match[1] : "";
		}

		var netName = getUrlParam("name");
		$(document).ready(function() {

            var country_code_select = document.getElementById("country_code");
            for (i = 0; i < countries.length; i ++) { 
                var val = countries[i].split(",");
                var option = document.createElement("option");
                option.value = val[0];
                option.text = val[2];
                country_code_select.add(option);
                country_code_select.value = 91;
            }
				$.ajax({
						url : link + 'checknetworkname/'+ netName,
						data : {},
						//async: true,
						type : 'GET',
						success : function(response) {
							var dto = response['dto'];
							var networkId = dto['id'];
							//////////////edited by Nivedita, Ankit, Rohit 
							globalNetId = networkId;
							/////////////////
							document.getElementById('saveid').value = networkId;
							document.getElementById('networkname').innerHTML = dto['title'];
                                                        document.getElementById('longDesc').title = dto['longDesc'];
									},
						error : function(retVal) {
							document.getElementById('networkname').innerHTML ="Network Not Found. You can still register.";
                                                        document.getElementById('longDesc').style.display = "none";
									}
							});
						})

		function callRegMember() {

			var networkId = $("#saveid").val();
			var phnumber = document.getElementById('mobileno').value;

				if (phnumber.toString().length>10 || phnumber.toString().length<10) 
				{
					alert("Please enter a valid Mobile Number");
				}
				else
				{
                    var country_code_select = document.getElementById("country_code");
					if (networkId!="") 
					{
						var networkMemberDto = new Object();
						networkMemberDto["networkId"] = networkId; 
				        console.log(country_code_select.value);
					    networkMemberDto["mobileNo"] = country_code_select.value+phnumber;
                        networkMemberDto["countryCode"] = country_code_select.value;
		                var dataVal = JSON.stringify(networkMemberDto);
                        console.log(dataVal);

			            $.ajax({
							url : link+'registertonetwork',
							data : dataVal,
							async : true,
							type : 'POST',
							contentType : "application/json; charset=utf-8",
							dataType : "json",
							success : function(response) {
								try {
									console.log(response);
									alert("You have been registered to the network. Please download or Open Huksa App.");
									} 
								catch (e) {
									alert("You could not be registered to the network. Please download Huksa App.");
									}
								},
							error : function(retVal) {
								alert("You could not be registered to the network. Please download Huksa App.");
								}
							}).done(function(result){
				 					var userAgent = navigator.userAgent || navigator.vendor || window.opera;
				  	  
				 					if (/android/i.test(userAgent)) {
				 					    window.location.href = "https://play.google.com/store/apps/details?id=com.huksalabs.app";
				 					    	}
				  
				 					else if (/iPad|iPhone|iPod/i.test(userAgent) && !window.MSStream) {
				 					    window.location.href = "https://itunes.apple.com/us/app/huksa/id1236562104";
				 					    }
				 					else
											window.location.href = "https://play.google.com/store/apps/details?id=com.huksalabs.app";
											// console.log("windows");
											// window.location.href ="/home/rohit/Downloads/Huksa-Landing-master/threads.html";
				 				  	});
					}
					else
					{
						var userAgent = navigator.userAgent || navigator.vendor || window.opera;
	  
					if (/android/i.test(userAgent)) {
					    window.location.href = "https://play.google.com/store/apps/details?id=com.huksalabs.app";
					    	}

					else if (/iPad|iPhone|iPod/i.test(userAgent) && !window.MSStream) {
					    window.location.href = "https://itunes.apple.com/us/app/huksa/id1236562104";
					 		}
					else
					{
						window.location.href = "https://play.google.com/store/apps/details?id=com.huksalabs.app";
						// window.location.href = "/home/rohit/Downloads/Huksa-Landing-master/threads.html";
					}		
			}
		}
	} 

//////////////Edited by Nivedita, Ankit, Rohit
function getThreads() {
	localStorage.setItem("netId", globalNetId);
	window.location.href = "/home/rohit/Downloads/Huksa-Landing-master/threads.html";
	
}