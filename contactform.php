<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 'on');

$url_google = "https://www.google.com/recaptcha/api/siteverify";
$secret_google = "6LfRpycUAAAAABmBMJkRRaS02w7FYlwY8PRUvdcW";
$response_captcha = $_POST['g-recaptcha-response'];
$remote_ip = $_SERVER["REMOTE_ADDR"];
$ch = curl_init();
$data_post = "secret=".$secret_google."&response=".$response_captcha."&remoteip=".$remote_ip;
//echo "data_post ".$data_post;
curl_setopt($ch, CURLOPT_URL,$url_google);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);
//echo "server_output ".$server_output;
curl_close ($ch);

$serverOutArray = json_decode($server_output, true);
//echo "output".$serverOutArray;
//echo "<br/>".$serverOutArray['success'];
// further processing ....
if ($serverOutArray['success'] == true) {
//echo "success ".$serverOutArray['success'];

//Fetching Values from URL
$name = $_POST['name'];
$email = $_POST['email'];
$message = $_POST['message'];
//sanitizing email
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
//After sanitization Validation is performed
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
$subject = $name;
// To send HTML mail, the Content-type header must be set
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: support@huksa.com' . "\r\n"; // Sender's Email
$headers .= 'To: huksasupport@huksa.com' . "\r\n"; // Sender's Email
$template = '<div style="padding:50px; color:white;">Hello ' . $name . ',<br/>'
. '<br/>Message Received.<br/><br/>'
. 'Name:' . $name . '<br/>'
. 'Email:' . $email . '<br/>'
. 'Message:' . $message . '<br/><br/><br/>'
. '<br/>';
$sendmessage = "<div style=\"background-color:#7E7E7E; color:white;\">" . $template . "</div>";
// message lines should not exceed 70 characters (PHP rule), so wrap it
$sendmessage = wordwrap($sendmessage, 70);

// Send mail by PHP Mail Function
mail("support@huksa.com", $subject, $sendmessage, $headers);
echo "Your query has been received, we will contact you soon.";
}
else {
echo "<span>* invalid email *</span>";
}
}
else {
   echo "Your captcha test fails.";
}
?>
